# WP Book
Download your posts, pages and custom post as a PDF Book in few clicks

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/b47f8c608de34f1582f3b5e48c9e5d40)](https://www.codacy.com/app/WPSmartCare/wp-book?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=rajanit2000/wp-book&amp;utm_campaign=Badge_Grade)

## Description

[Wp Book](https://wpbook.wpsmartcare.com/?utm_source=gitlab.com&utm_medium=website&utm_campaign=Repository) is a WordPress Plugin that allows you to create PDF book for your posts, pages custom posts on different conditions and filters.

### Features

The following conditions and filters are supported.

#### Post Filters options

- Post type
- Number of posts
- Post order
- Post order by

#### PDF Layout options

- Post type
- Number of posts
- Post order
- Post order by

#### Sorting

- Support manual sort in UI

## Installation

The simplest way to install the plugin is to use the built-in automatic plugin installer. Go to plugins -> Add New and then enter the name of the plugin to automatically install it.

If for some reason the above method doesn't work then you can download the plugin as a zip file, extract it and then use your favorite FTP client and then upload the contents of the zip file to the wp-content/plugins/ directory of your WordPress installation and then activate the Plugin from Plugins page.

Goto `Settings > WP Book`

### Development Environment 

Need following tools for setup devloping enveronment 

* [NodeJS](https://nodejs.org/en/), 
* [Composer](https://getcomposer.org/). 

Once you install this things then run following commands on your terminal  

`git clone https://gitlab.com/rajanit2000/wp-book.git`  
`cd wp-book`  
`npm install`  
`composer install`  
`gulp dist`  

## Credits

Thanks to [mPDF](https://mpdf.github.io/) PHP library, create pdf using PHP.  
Thanks to [jQueryUI](https://jqueryui.com/) JavaScript library, which the Plugin uses.  
Thanks to [select2](https://select2.github.io/) JavaScript library, which the Plugin uses.  

## Changelog 
1.0 
* Initial release.